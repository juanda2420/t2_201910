package tetst;

import java.util.LinkedList;

import junit.framework.TestCase;

public class LinkedListTest extends TestCase {

	LinkedList<Integer> arreglo = new LinkedList<Integer>();

	/**
	 * Arreglo con los elementos del escenario
	 */
	protected static final int[] ARREGLO_ESCENARIO = { 350, 383, 105, 233, 140, 266, 356, 236, 80, 360, 221, 241, 130,
			244, 352, 446, 18, 98, 97, 396 };

	public void setupEscenario1() {
		for (int actual : ARREGLO_ESCENARIO) {
			arreglo.add(actual);
		}

	}

	public void testAdd() throws Exception {
		// Prueba la lista vac�a.
		assertTrue("La lista debe estar vacia", arreglo.isEmpty());
		assertEquals("no es 0", 0, arreglo.size());

		// Agrega dos elementos.
		assertTrue("No agrega el elemento correctamente", arreglo.add(5));
		assertTrue("No agrega el elemento correctamente", arreglo.add(30));
		assertFalse("La lista no deberia estar vacia", arreglo.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, arreglo.size());
		assertTrue("La lista no contiene 5", arreglo.contains(5));

		// Agrega 20 elementos.
		arreglo.clear();
		setupEscenario1();

		assertFalse("La lista no es vacia", arreglo.isEmpty());
		assertEquals("La lista debe tener 20 elementos", ARREGLO_ESCENARIO.length, arreglo.size());

	}

	public void testAddEnd() {

		setupEscenario1();
		arreglo.addLast(50);
		assertEquals(arreglo.size(), 21);
		assertEquals(arreglo.get(20).intValue(), 50);

	}

	public void testAddATK() {
		setupEscenario1();
		arreglo.add(5, 40);

		assertEquals(arreglo.get(5).intValue(), 40);
	}

	public void testGetElement() {
		// Revisa la lista vac�a.
		try {
			arreglo.get(0);
			fail("Deber�a lanzar excepci�n porque la lista est� vac�a.");
		} catch (IndexOutOfBoundsException e) {
			// Deber�a lanzar la excepci�n
		}

		setupEscenario1();

		try {
			arreglo.get(-1);
			fail("Deber�a lanzar excepci�n porque el indice no existe");
		} catch (IndexOutOfBoundsException e) {
			// Deber�a lanzar la excepci�n
		}

		try {
			arreglo.get(500);
			fail("Deber�a lanzar excepci�n porque el indice esta por fuera de la lista");
		} catch (IndexOutOfBoundsException e) {
			// Deber�a lanzarla
		}

		for (int i = 0; i < ARREGLO_ESCENARIO.length; i++) {
			try {
				Integer elemento = arreglo.get(i);
				assertNotNull("Los elementos recuperados no pueden ser nulos", elemento);
			} catch (IndexOutOfBoundsException e) {
				fail("El elemento se encuentra dentro del rango, no deberia lanzar excepcion");
			}
		}
	}

	public void testGetSize() {
		// Prueba la lista vac�a.
		assertEquals("El tamaño de la lista vacia no es correcto", 0, arreglo.size());

		// Prueba la lista con dos elementos

		arreglo.add(5);
		arreglo.add(30);

		assertEquals("El tama�o de la lista con dos elementos no es correcto", 2, arreglo.size());

		// Prueba vaciando la lista
		arreglo.clear();
		assertEquals("El tama�o de la lista vac�a no es correcto", 0, arreglo.size());

		// Prueba la lista con 20 elementos
		setupEscenario1();

		assertEquals("El tama�o de la lista con 20 elementos no es correcto", ARREGLO_ESCENARIO.length,
				arreglo.size());

	}

	public void tetstDeleteATK() {
		// Prueba con la lista vac�a.
		try {
			arreglo.remove(0);
			fail("No deber�a dejar eliminar elementos porque la lista est� vac�a");
		}
		catch (IndexOutOfBoundsException e) {

		}

		try {
			arreglo.remove(-1);
			fail("No deber�a eliminar porque el indice es negativo");
		} catch (IndexOutOfBoundsException e) {

		}

		// Prueba con 20 elementos
		setupEscenario1();
		try {
			arreglo.remove(-1);
			fail("No deber�a dejar eliminar porque el �ndice es negativo");
		} catch (IndexOutOfBoundsException e) {

		}

		try {
			arreglo.remove(arreglo.size());
			fail("No deber�a dejar eliminar porque el �ndice est� por fuera de la lista");
		} catch (IndexOutOfBoundsException e) {

		}
	}

}
