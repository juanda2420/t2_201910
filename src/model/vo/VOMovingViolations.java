package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int objectID;
	private int Row;
	private String Location;
	private int Adress_ID;
	private int StreerSEID;
	private double Xcoord;
	private double Ycoord;
	private String TicketType;
	private short FineAMT;
	private short TotalPaid;
	private short penalty1;
	private short penalty2;
	private String AccidentIndicator;
	private String TicketIssueDate;
	private String ViolationIndicator;
	private String ViolationDesc;

	public VOMovingViolations(String pObjectID, String pRow, String pLocation, String pAdress, String pStreerSEID,
			String pXcoord, String pYcoord, String pTicketType, String pFineAMT, String pTotalPaid, String pPenalty1,
			String pPenalty2, String pAccidentIndicator, String pTicketIssueDate, String pViolationIndicator,
			String pViolationDesc) {

		objectID = Integer.parseInt(pObjectID);
		Row = Integer.parseInt(pRow);
		Location = pLocation;
		Adress_ID = Integer.parseInt(pAdress);
		StreerSEID = Integer.parseInt(pStreerSEID);
		Xcoord = Double.parseDouble(pXcoord);
		Ycoord = Double.parseDouble(pYcoord);
		TicketType = pTicketType;
		FineAMT = Short.parseShort(pFineAMT);
		TotalPaid = Short.parseShort(pTotalPaid);
		penalty1 = Short.parseShort(pPenalty1);
		penalty2 = Short.parseShort(pPenalty2);
		AccidentIndicator = pAccidentIndicator;
		TicketIssueDate = pTicketIssueDate;
		ViolationIndicator = pViolationIndicator;
		ViolationDesc = pViolationDesc;

	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		return objectID;
	}

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return Location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return TicketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la
	 *         infracción en USD.
	 */
	public int getTotalPaid() {
		return TotalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String getAccidentIndicator() {
		return AccidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String getViolationDescription() {
		return ViolationIndicator;
	}

	public int getRow() {
		return Row;
	}

	public int getAdress_ID() {
		return Adress_ID;
	}

	public Double getXcoord() {
		return Xcoord;
	}

	public Double getYcoord() {
		return Ycoord;
	}

	public String getTicketType() {
		return TicketType;
	}

	public int getFineAMT() {
		return FineAMT;
	}

	public short getPenalty1() {
		return penalty1;
	}

	public short getPenalty2() {
		return penalty2;
	}

	public String getViolationDesc() {
		return ViolationDesc;
	}

	public int getStreetGID() {
		return getStreetGID();
	}
	
	

}
