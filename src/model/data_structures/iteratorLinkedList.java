package model.data_structures;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import com.sun.xml.internal.bind.v2.runtime.reflect.ListIterator;


public class iteratorLinkedList <T> implements Iterator<T>, Serializable {

	/**
	 * Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;


	private Node<T> anterior;


	private Node<T> actual;


	public iteratorLinkedList (Node<T> primerNodo) {
		actual = primerNodo;
		anterior = null;
	}

	/**
	 * Indica si hay nodo siguiente true en caso que haya nodo siguiente o false en
	 * caso contrario
	 */
	public boolean hasNext() {

		return actual != null;
	}

	/**
	 * Indica si hay nodo anterior true en caso que haya nodo anterior o false en
	 * caso contrario
	 */
	public boolean hasPrevious() {
		return anterior != null;
	}

	/**
	 * Devuelve el elemento siguiente de la iteraci�n y avanza.
	 * 
	 * @return elemento siguiente de la iteraci�n
	 * @throws NoSuchElementException Se lanza en caso de que se pida el siguiente y
	 *                                ya se encuentre al final de la lista
	 */
	public T next() throws NoSuchElementException {
		T RTA = null;

		if (hasNext() == false) {
			throw new NoSuchElementException("El siguiente de la lista es nulo");
		} else {

			Node<T> act = actual;
			Node<T> sig = (Node<T>) actual.darSiguiente();

			RTA = actual.darElemento();

			actual = sig;
			anterior = act;

		}
		return RTA;

	}

	/**
	 * Devuelve el elemento anterior de la iteraci�n y retrocede.
	 * 
	 * @return elemento anterior de la iteraci�n.
	 * @throws NoSuchElementException Se lanza en caso de que se pida el anterior y
	 *                                ya se encuentra al final de la lista.
	 */
	public T previous() throws NoSuchElementException {

		T RTA = null;

		if (hasPrevious() == false) {
			throw new NoSuchElementException("El elemento anterior es nulo");
		}

		else {
			RTA = anterior.darElemento();

			actual = anterior;
			anterior = anterior.darAnterior();

		}

		return RTA;

	}

	// =======================================================
	// M�todos que no se implementar�n
	// =======================================================

	public int nextIndex() {
		throw new UnsupportedOperationException();
	}

	public int previousIndex() {
		throw new UnsupportedOperationException();
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}

	public void set(T e) {
		throw new UnsupportedOperationException();
	}

	public void add(T e) {
		throw new UnsupportedOperationException();
	}

}
