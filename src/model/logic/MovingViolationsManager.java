package model.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

public class MovingViolationsManager implements IMovingViolationsManager {

	private LinkedList<VOMovingViolations> linkedlistAct = new LinkedList<VOMovingViolations>();

	public void loadMovingViolations(String movingViolationsFile) {

		File archivo = new File(movingViolationsFile);
		int contador = 0;
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");

				StringTokenizer st = new StringTokenizer(linea, ",");

				ArrayList<String> datosActual = new ArrayList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevo = new VOMovingViolations(datosActual.get(0), datosActual.get(1),
						datosActual.get(2), datosActual.get(3), datosActual.get(4), datosActual.get(5),
						datosActual.get(6), datosActual.get(7), datosActual.get(8), datosActual.get(9),
						datosActual.get(10), datosActual.get(11), datosActual.get(12), datosActual.get(13),
						datosActual.get(14), datosActual.get(15));
				linkedlistAct.add(nuevo);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public LinkedList<VOMovingViolations> getMovingViolationsByViolationCode(String violationCode) {

		LinkedList<VOMovingViolations> RTA = new LinkedList<VOMovingViolations>();

		Iterator it = linkedlistAct.iterator();

		while (it.hasNext()) {

			VOMovingViolations actual = (VOMovingViolations) it.next();

			if (actual.getViolationDescription().equals(violationCode)) {

				RTA.add(actual);

			}

		}

		return RTA;

	}

	public LinkedList<VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {

		LinkedList<VOMovingViolations> RTA = new LinkedList<VOMovingViolations>();

		System.out.println();

		Iterator it = linkedlistAct.iterator();

		while (it.hasNext()) {

			VOMovingViolations actual = (VOMovingViolations) it.next();

			if (actual.getAccidentIndicator().equals(accidentIndicator)) {

				RTA.add(actual);

			}

		}

		return RTA;

	}

}
